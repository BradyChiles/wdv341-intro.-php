<?php
	session_start();

	if($_SESSION['validUser'] != true){
		header('Location: loginPage.php');
	}
?>

<!DOCTYPE html>
<html>

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Events</title>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

		<!-- Bootstrap 4 -->
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<!-- Popper JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

		<style>
			table{

				margin: 100px auto auto auto;
			}
		</style>



		<?php

			if(isset($_SESSION['deleteSuccess'])){
				if( $_SESSION['deleteSuccess'] == true){
					echo "<script>alert('Delete Successful!');</script>";
				}else{
					echo "<script>alert('Problem with Delete...');</script>";
				}
				unset($_SESSION['deleteSuccess']);
			}

			include 'connectPDO.php';

			if($connectionSuccess == "Connected successfully"){

				// prepare sql and bind parameters
			    $stmt = $conn->prepare("SELECT * FROM wdv341_event");
			    if($stmt->execute()){
	   ?>

	   <script>
	   	
	   	$(document).ready( function(){

	   		$('a.deleteLink').click( function(event){
		   		var confirmation = confirm("Are you absolutely positive you would like to delete this record?");
		   		if(!confirmation){
		   			alert("Deletion Cancelled!");
		   			event.preventDefault();
		   		}
		   	});

	   	});
	   	

	   </script>

	</head>


	<body>

		<nav class="navbar navbar-expand-xl bg-dark navbar-dark fixed-top row">
	    	<div class="collapse navbar-collapse ml-auto" id="collapsibleNavbar">
	    		<ul class="navbar-nav ml-auto">
	    			<li class="nav-item">
	    				<a class="nav-link active" href="logoutPage.php">Logout</a>
	    			</li>
	    	
	    		</ul>
	    	</div> 
	    </nav>

		<table border='1'>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Description</th>
				<th>Presenter</th>
				<th>Date</th>
				<th>Time</th>
				<th>Update</th>
				<th>Delete</th>
			<tr>

			<?php 

				$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
				foreach($stmt->fetchAll() as $row){
					echo "<tr>";
						echo "<td>" . $row['event_id'] . "</td>";
						echo "<td>" . $row['event_name'] . "</td>";	
						echo "<td>" . $row['event_description'] . "</td>";
						echo "<td>" . $row['event_presenter'] . "</td>";
						echo "<td>" . $row['event_date'] . "</td>";
						echo "<td>" . substr($row['event_time'],0,-3) . "</td>";
						echo "<td><a class='updateLink' href='updateEventForm.php?recordID=" . $row['event_id'] . "'>Update</a></td>";	
						echo "<td><a class='deleteLink' href='deleteEvent.php?recordID=" . $row['event_id'] . "'>Delete</a></td>";
						
					echo "</tr>";
				}

			?>

		</table>

		<?php 

			}
		}

		?>

	</body>

</html>