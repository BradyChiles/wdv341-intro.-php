<?php
  session_start();

  if($_SESSION['validUser'] != true){
    header('Location: loginPage.php');
  }
?>

<!DOCTYPE html>

<html>

  <head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Logout Page</title>

    <!-- Bootstrap 4 -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <style>

      #logoutHeader{
        text-align: center;
        width: 30%;
        margin: 150px auto 30px auto;
      }

      #logoutButton{
        display: block;
        margin: auto;
      }

    </style>

    <?php

      if(isset($_POST['logoutButton'])){
        unset($_SESSION['validUser']);
        header('Location: loginPage.php');
      }
    ?>

  </head>

  <body>

    <div id="logoutContent">
      <h1 id="logoutHeader">Logout</h1>
      <form id="logoutForm" action="logoutPage.php" method="post">
        <input type="submit" id="logoutButton" name="logoutButton" value="Leave">
      </form>
    </div>

  </body>

</html>