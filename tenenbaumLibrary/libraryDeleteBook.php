<?php
	session_start();

	if($_SESSION['validUser'] != true){
		header('Location: libraryLoginPage.php');
	}

	include 'connectPDO.php';

	$recordID = $_GET['recordID'];

	if($connectionSuccess == true){

			// prepare sql and bind parameters
		$stmt = $conn->prepare("DELETE FROM library_books WHERE book_id = :bookID");
		$stmt->bindParam(':bookID', $recordID);

		$_SESSION['deleteSuccess'] = $stmt->execute();
		header('Location: libraryBookDisplay.php');
	}

?>