<?php
$serverName = "localhost";
$username = "root";
$password = "Sandman2494";
$database = "wdv341";
$connectionSuccess;
$connectionMessage = "";

try {
    $conn = new PDO("mysql:host=$serverName;dbname=$database", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $connectionSuccess = true; 
    }
catch(PDOException $e)
    {
    	$connectionSuccess = false;
     	$connectionMessage= "Connection failed: " . $e->getMessage();
    }
?>