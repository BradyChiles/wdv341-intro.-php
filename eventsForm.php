<?php
	session_start();

	if($_SESSION['validUser'] != true){
		header('Location: loginPage.php');
	}
?>

<!DOCTYPE html>
<html>

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Insert Event</title>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src='https://www.google.com/recaptcha/api.js'></script>

		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

		<?php

			include 'connectPDO.php';

			if($connectionSuccess == "Connected successfully"){

				// prepare sql and bind parameters
			    $stmt = $conn->prepare("INSERT INTO wdv341_event (event_name, event_description, event_presenter, event_date, event_time) 
			    VALUES (:eventName, :eventDescription, :eventPresenter, :eventDate, :eventTime)");
			    $stmt->bindParam(':eventName', $eventName);
			    $stmt->bindParam(':eventDescription', $eventDescription);
			    $stmt->bindParam(':eventPresenter', $eventPresenter);
			    $stmt->bindParam(':eventDate', $eventDate);
			    $stmt->bindParam(':eventTime', $eventTime);

				//Variables
				$eventName = "";
				$eventDescription = "";
				$eventPresenter = "";
				$eventDate = "";
				$eventTime = "";

				//error variables
				$eventNameError = "";
				$eventDescriptionError = "";
				$eventPresenterError = "";
				$eventDateError = "";
				$eventTimeError = "";
				$captchaError = "";

				$validForm = false;
				$message = "";


				function validateEventName($eventName){
					global $validForm, $eventNameError;	
					$eventNameError = "";

					if($eventName == ""){
						$validForm = false;
						$eventNameError = "Event Name is required";
					}else if(!preg_match('/^[A-Za-z0-9 ,.]*[A-Za-z0-9][A-Za-z0-9 ,.]*$/', $eventName)){
						$validForm = false;
						$eventNameError = "Event Name cannot contain special characters";
					}
				}

				function validateEventDescription($eventDescription){
					global $validForm, $eventDescriptionError;	
					$eventDescriptionError = "";

					if($eventDescription == ""){
						$validForm = false;
						$eventDescriptionError = "Event Description is required";
					}
				}

				function validateEventPresenter($eventPresenter){
					global $validForm, $eventPresenterError;	
					$eventPresenterError = "";

					if($eventPresenter == ""){
						$validForm = false;
						$eventPresenterError = "Event Presenter is required";
					}else if(!preg_match('/^[A-Za-z0-9 ,.]*[A-Za-z0-9][A-Za-z0-9 ,.]*$/', $eventPresenter)){
						$validForm = false;
						$eventPresenterError = "Event Presenter cannot contain special characters";
					}
				}

				function validateEventDate($eventDate){
					global $validForm, $eventDateError;	
					$eventDateError = "";

					if($eventDate == ""){
						$validForm = false;
						$eventDateError = "Event Date is required";
					}
				}

				function validateEventTime($eventTime){
					global $validForm, $eventTimeError;	
					$eventTimeError = "";

					if($eventTime == ""){
						$validForm = false;
						$eventTimeError = "Event Time is required";
					}else if(!preg_match('/^([01]\d|2[0-3]):?([0-5]\d)$/', $eventTime)){
						$validForm = false;
						$eventTimeError = "Must match format: 00:00 - 23:59";
					}
				}

				// reCaptcha
				function validateRecaptcha($captcha){
					global $validForm, $captchaError;
					$captchaError = "";

					$secretKey = "6Le3REoUAAAAAIVoaYSGnA23Jcft4Ey5IzvojUIo";
					$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha);
					$responseKeys = json_decode($response,true);

					if($responseKeys["success"] != true) {
						$validForm = false;
						$captchaError = "Please complete the reCaptcha";
					}

					grecaptcha.reset();
				}

				if(isset($_POST["submit"]))
				{

					$eventName = $_POST['event_name'];
					$eventDescription = $_POST['event_description'];
					$eventPresenter = $_POST['event_presenter'];

					//COnvert string to a timestamp, and then timestamp into a date
					$timestamp = strtotime($_POST['event_date']);
					$eventDate = date("Y-m-d", $timestamp);

					$eventTime = $_POST['event_time'];
					if(isset($_POST['g-recaptcha-response'])){
						$captcha = $_POST['g-recaptcha-response'];
					}

					$validForm = true;

					validateEventName($eventName);
					validateEventDescription($eventDescription);
					validateEventPresenter($eventPresenter);
					validateEventDate($eventDate);
					validateEventTime($eventTime);
					validateRecaptcha($captcha);


					if($validForm == false){
						$message = "The form is incomplete";
					}else{
					    $stmt->execute();

						$message = "Event Entered!";
						$eventName = "";
						$eventDescription = "";
						$eventPresenter = "";
						$eventDate = "";
						$eventTime = "";

						$conn = null;
					}
					
				}
			}
			
		?>

		<script>
			$(document).ready( function(){

				$('#resetForm').click(function(){
					$("#event_name").val("");
					$("#event_description").val("");
					$("#event_presenter").val("");
					$("#event_date").val("");
					$("#event_time").val("");
					$('.error').html("");
				});

			});

			$( function() {
			    $( "#event_date" ).datepicker({ minDate: 0});
			});
			
		</script>

		<style>

			#message{
				text-align: center;
				margin: 15px auto;
			}
			
			form{
				text-align: center;
			}
			

			.form {
				background-color:white;
				padding-left: 5em;
			}

			table{
				margin: auto;
			}
			table td{
				padding-bottom: .75em;
			}
			.error{
				font-style: italic;
				color: #d42a58;
				font-weight: bold;
			}

			@media only screen and (max-width:620px) {
				.form {
					width:100%; 
					padding-left: .1em;
					padding-top: .1em;
				}
				table{
					margin: auto;
				}
				table td{
					padding-bottom: .5em;
				}
			}

		</style>

	</head>


	<body>

		<h2 id="message"><?php echo $message; ?></h2>

		<form id="eventsForm" method="post" action="eventsForm.php">
			
			<table>
				<tr>
					<td>Event Name:<br> <input type="text" id="event_name" name="event_name" value="<?php echo $eventName; ?>"/><br><span class="error" id="eventNameError"><?php echo($eventNameError);?></span></td>
				</tr>
				<tr>
					<td>Event Description:<br> <input type="text" id="event_description" name="event_description" value="<?php echo $eventDescription; ?>"/><br><span class="error" id="eventDescriptionError"><?php echo($eventDescriptionError);?></span></td>
				</tr>
				<tr>
					<td>Presenter:<br> <input type="text" id="event_presenter" name="event_presenter" value="<?php echo $eventPresenter; ?>" /><br><span class="error" id="eventPresenterError"><?php echo($eventPresenterError);?></span></td>
				</tr>
				<tr>
					<td>Event Date:<br> <input type="text" id="event_date" name="event_date" value="<?php echo $eventDate ?>" /><br><span class="error" id="eventDateError"><?php echo($eventDateError);?></span><td>
				</tr>
				<tr>
					<td>Event Time:<br> <input type="text" id="event_time" name="event_time" value="<?php echo $eventTime ?>" /><br><span class="error" id="eventTimeError"><?php echo($eventTimeError);?></span><td>
				</tr>
					
				<tr>
					<td><div class="g-recaptcha" data-sitekey="6Le3REoUAAAAAEJ8etrsYzKL7ZM9ZwMRldJCQ8tz"></div>
					<span class="error"><?php echo $captchaError; ?></span></td>

					<input type="hidden" id="submitConfirm" name="submitConfirm" value="submitConfirm"/>
				</tr>
				<tr>
					<td>
						<input type="submit" id="submitEvent" name="submit" value="Submit Event" />
						<input type="button" id="resetForm" name="reset" value="Reset Form"/>
					</td>
				</tr>
			</table>
		</form>

	</body>

</html>