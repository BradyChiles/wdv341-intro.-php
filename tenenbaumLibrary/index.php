<?php
  session_start();
  $_SESSION['validUser'] = false;
?>

<!doctype html>
<html class="no-js" lang="en" dir="ltr">

<head>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Tenenbaum Royal Library</title>

  <!--      Foundation Styles-->
  <link rel="stylesheet" href="css/foundation.css">
  <link rel="stylesheet" href="css/app.css">

  <!--      Icon Library-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!--      Custom Styles-->
  <link rel="stylesheet" href="navigationBarStyles.css">
  <link rel="stylesheet" href="mainStyles.css">
  <link rel="stylesheet" href="bookSaleStyles.css">
  <link rel="stylesheet" href="navigationBarStyles.scss">
  <link rel="stylesheet" href="mainStyles.scss">
  <link rel="stylesheet" href="bookSaleStyles.scss">

  <!--      Favicon-->
  <link rel="icon" href="images/book-logo-design-vector-15225895_clipped_rev_1.png">

  <style>

  body, html{
    height: 100%;
    width: 100%;
  }




</style>

</head>

<body>
  <div class="off-canvas-wrapper">

    <!--          Begin Navigation-->

    <div class="off-canvas position-right" id="offCanvas" data-off-canvas>

      <!-- Menu -->
      <ul class="vertical menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="#">Our Collection</a></li>
        <li><a href="#">Events &amp; News</a></li>
        <li><a href="#">Locations &amp; Hours</a></li>
        <li><a href="libraryContactPage.php">Contact</a></li>
        <?php
          if(isset($_SESSION['validUser'])){
            if($_SESSION['validUser']){
              echo "<li><a href='libraryBookDisplay.php'>Book Inventory</a></li>";
              echo "<li><a href='libraryAddBook.php'>Add Book</a></li>";
              echo "<li><a href='libraryLogoutPage.php'>Logout</a></li>";
            }else{
              echo "<li><a href='libraryLoginPage.php'>Admin Login</a></li>";
            }
          }
        ?>
      </ul>

    </div>

    <!--          End Navigation-->


    <!--          Begin Body Content-->
    <div class="off-canvas-content" data-off-canvas-content>  
      <div id="menuToggle" data-toggle="offCanvas">
       <button type="button" class="menu-icon dark" ></button>
     </div>


     <!--            Begin Header-->

     <div id="header">
      <h1 id="mainHeaderText">Tenenbaum Royal Library</h1>

      <div id="headerImage">
        <div id="searchBar">
          <p id="searchBarHeading">WHAT ARE YOU LOOKING FOR?</p>
          <form>
            <div class="input-group">
              <input class="input-group-field" type="text" placeholder="Find books, music, and more...">
              <div class="input-group-button">
                <input type="button" class="button" value="Search">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!--            End Header-->


    <!--            Begin Event Section-->

    <div id="eventsSection">

      <h2 id="eventsHeader">Upcoming Events</h2>

      <div class="grid-x grid-padding-x eventItem">  
        <div class="large-3 cell">
          <div class="blueBalls">
            Dec <br> 10
          </div>
        </div>
        <div class="large-9 cell eventInformation">
          <a href="#">Super Fun Sunday</a>
          <br>
          1:00pm <br> Central Library
        </div>
      </div>

      <div class="grid-x grid-padding-x eventItem">  
        <div class="large-3 cell">
          <div class="blueBalls">
            Dec <br> 11
          </div>
        </div>
        <div class="large-9  cell eventInformation">
          <a href="#">Baby Rhyme Time</a>
          <br>
          10:15am <br> Forest Library
        </div>
      </div>

      <div class="grid-x grid-padding-x eventItem">  
        <div class="large-3 cell">
          <div class="blueBalls">
            Dec <br> 11
          </div>
        </div>
        <div class="large-9 cell eventInformation">
          <a href="#">Comics Creators</a>
          <br>
          3:00pm <br> Forest Library
        </div>
      </div>


      <div class="grid-x grid-padding-x eventItem">  
        <div class="large-3 cell">
          <div class="blueBalls">
            Dec <br> 12
          </div>
        </div>
        <div class="large-9 cell eventInformation">
          <a href="#">Family Story Time</a>
          <br>
          6:30pm <br> South Library
        </div>
      </div>

      <div class="grid-x grid-padding-x eventItem">  
        <div class="large-3 cell">
          <div class="blueBalls">
            Dec <br> 13
          </div>
        </div>
        <div class="large-9 cell eventInformation">
          <a href="#">Mother Goose on the Loose</a>
          <br>
          10:30am <br> East Library
        </div>
      </div>

      <div class="grid-x grid-padding-x eventItem">  
        <div class="large-3 cell">
          <div class="blueBalls">
            Dec <br> 13
          </div>
        </div>
        <div class="large-9 cell eventInformation">
          <a href="#">Furry Tales</a>
          <br>
          11:00am <br> Central Library
        </div>
      </div>
    </div>

    <!--            End Event Section-->


    <!--            Begin Main Section-->

    <div id="mainSection">

      <div class="grid-x grid-padding-x">
        <div class="large-offset-1 large-5 cell" style="background-color: #fffeee;"><img src="images/simplesteps-homepage-brickv3.png"></div>
        <div class="large-5 cell" style="background-color: #66c7c9;">
          <img src="images/lyndabrickv6.png">
          <div>Learn a new skill online!</div>
        </div>
      </div>

      <div class="grid-x grid-padding-x">
        <div class="large-offset-1 large-5 cell" style="background-color: #f2f3e2;">
          <img src="images/shelfbrickv2.jpg">
          <div>Find your next great read!</div>
        </div>
        <div class="large-5 cell" style="background-color: #b6325c;"><img src="images/libbybrickv1.jpg"></div>
      </div>
    </div>

    <!--            End Main Section-->





    <!--            Begin Book Sale-->

    <div id="bookSaleSection">
      <a href="#">
        <div id="bookSale">
          <div id="bookSaleText">
            <p id="bookSaleHeader">Used Book Sale!</p>
            <p id="bookSalePrices">
              Hardcovers  -  $2.99 <br>
              Paperbacks  -  $1.00 <br>
              Textbooks   -  $4.99 <br>
              Magazines   -  $0.50 <br>
              DVDs        -  $2.00 <br>
              CDs         -  $1.50 <br>
            </p>
            <p id="bookSaleDates"> Dec 15 <br> - <br> Dec 23</p>
          </div>

          <img id="bovary" src="images/madameBovary.jpg" alt="Madame Bovary Gustave Falubert">
          <img id="expectations" src="images/greatExpectations.jpg" alt="Great Expectations Charles Dickins">
          <img id="wuthering" src="images/wutheringHeights.jpg" alt="Wuthering Heights Emily Bronte">
          <img id="sensibility" src="images/senseAndSensibility.jpg" alt="Sense and Sensibility Jane Austen">
          <img id="cranford" src="images/cranford.jpg" alt="Cranford Elizabeth Gaskell">
          <img id="durbervilles" src="images/tessDurbervilles.jpg" alt="Tess of the d'Urbervilles Thomas Hardy">
          <img id="prejudice" src="images/pridePrejudice.jpg" alt="Pride and Prejudice Jane Austen">
          <img id="punishment" src="images/crimePunishment.jpg" alt="Crime and Punishemtn Pyodor Dostoyevsky">
          <img id="eyre" src="images/janeEyre.jpg" alt="Jane Eyre Charlotte Bronte">
          <img id="dorian" src="images/dorianGray.jpg" alt="The Picture of Dorian Gray Oscar Wilde">
          <img id="bovary2" src="images/madameBovary.jpg" alt="Madame Bovary Gustave Falubert">
          <img id="expectations2" src="images/greatExpectations.jpg" alt="Great Expectations Charles Dickins">
          <img id="wuthering2" src="images/wutheringHeights.jpg" alt="Wuthering Heights Emily Bronte">
          <img id="sensibility2" src="images/senseAndSensibility.jpg" alt="Sense and Sensibility Jane Austen">
          <img id="cranford2" src="images/cranford.jpg" alt="Cranford Elizabeth Gaskell">
        </div>
      </a>
    </div>

    <!--        Begin Horizontal Book Sale-->

    <div id="horizontalBookSale">
      <p id="horizontalSaleHeader">Used Book Sale!</p>
      <p id="horizontalSalePrices">
        <span id="hardcovers">Hardcovers &nbsp;$2.99&nbsp; * &nbsp;</span>
        <span id="paperbacks">Paperbacks &nbsp;$1.00&nbsp; * &nbsp;</span>
        <span id="textbooks">Textbooks &nbsp;$4.99 </span> <br>
        <span id="magazines">Magazines&nbsp; $0.50&nbsp; * &nbsp;</span>
        <span id="dvds">DVDs &nbsp;$2.00&nbsp; * &nbsp;</span>
        <span id="cds">CDs &nbsp;$1.50</span>
      </p>
      <p id="horizontalSaleDates">Dec 15 - Dec 23</p>


      <img id="bovary" src="images/madameBovary.jpg" alt="Madame Bovary Gustave Falubert">
      <img id="expectations" src="images/greatExpectations.jpg" alt="Great Expectations Charles Dickins">
      <img id="wuthering" src="images/wutheringHeights.jpg" alt="Wuthering Heights Emily Bronte">
      <img id="sensibility" src="images/senseAndSensibility.jpg" alt="Sense and Sensibility Jane Austen">
      <img id="cranford" src="images/cranford.jpg" alt="Cranford Elizabeth Gaskell">
      <img id="durbervilles" src="images/tessDurbervilles.jpg" alt="Tess of the d'Urbervilles Thomas Hardy">
      <img id="prejudice" src="images/pridePrejudice.jpg" alt="Pride and Prejudice Jane Austen">
      <img id="punishment" src="images/crimePunishment.jpg" alt="Crime and Punishemtn Pyodor Dostoyevsky">
      <img id="eyre" src="images/janeEyre.jpg" alt="Jane Eyre Charlotte Bronte">
      <img id="dorian" src="images/dorianGray.jpg" alt="The Picture of Dorian Gray Oscar Wilde">
      <img id="bovary2" src="images/madameBovary.jpg" alt="Madame Bovary Gustave Falubert">
      <img id="expectations2" src="images/greatExpectations.jpg" alt="Great Expectations Charles Dickins">
      <img id="wuthering2" src="images/wutheringHeights.jpg" alt="Wuthering Heights Emily Bronte">
      <img id="sensibility2" src="images/senseAndSensibility.jpg" alt="Sense and Sensibility Jane Austen">
      <img id="cranford2" src="images/cranford.jpg" alt="Cranford Elizabeth Gaskell">
    </div>

    <!--        End Horizontal Book Sale-->

    <!--            End Book Sale-->


    <!--            Begin Footer-->

    <div id="footer">
      <div id="libraryHours" class="grid-x grid-padding-x">

        <div class="large-offset-2 large-2 medium-6 cell">
          <a href="#">Central</a> <br>
          1000 Grand Avenue, 50309 <br>
          <span class="boldText">Mon – Wed:</span> 9 AM – 8 PM <br> 
          <span class="boldText">Thu – Fri:</span> 9 AM – 6 PM <br>
          <span class="boldText">Sat:</span> Closed <br>
          <span class="boldText">Sun:</span> 1 PM - 5 PM [Sep - May] <br>
          <span class="boldText">Sun:</span> Closed [Jun - Aug]
        </div>

        <div class="large-2 medium-6 cell">
          <a href="#">Forest</a> <br>
          1326 Forest Avenue, 50314 <br>
          <span class="boldText">Mon:</span> 10 AM – 8 PM <br>
          <span class="boldText">Tue – Wed:</span> 10 AM – 6 PM <br>
          <span class="boldText">Thu:</span> 10 AM – 8 PM <br>
          <span class="boldText">Fri:</span> Closed <br>
          <span class="boldText">Sat:</span> 10 AM – 5 PM <br>
          <span class="boldText">Sun:</span> Closed
        </div>

        <div class="large-2 medium-6 cell">
          <a href="#">South Side</a> <br>
          1111 Porter Avenue, 50315 <br>
          <span class="boldText">Mon:</span> 10 AM – 8 PM <br>
          <span class="boldText">Tue:</span> Closed <br>
          <span class="boldText">Wed:</span> 10 AM – 8 PM <br>
          <span class="boldText">Thu – Fri:</span> 10 AM – 6 PM <br>
          <span class="boldText">Sat:</span> 10 AM – 5 PM <br>
          <span class="boldText">Sun:</span> Closed
        </div>

        <div class="large-2 medium-6 cell">
          <a href="#">East Side</a> <br>
          2559 Hubbell Avenue, 50317 <br>
          <span class="boldText">Mon – Tue:</span> 10 AM – 8 PM <br>
          <span class="boldText">Wed:</span> Closed <br>
          <span class="boldText">Thu – Fri:</span> 10 AM – 6 PM <br>
          <span class="boldText">Sat:</span> 10 AM – 5 PM <br>
          <span class="boldText">Sun:</span> Closed
        </div>
      </div>

      <div id="socialMediaIcons">
        <a href="#" class="fa fa-facebook"></a>
        <a href="#" class="fa fa-twitter"></a>
        <a href="#" class="fa fa-instagram"></a>
        <a href="#" class="fa fa-snapchat-ghost"></a>
        <a href="#" class="fa fa-youtube"></a>
      </div>

    </div>

    <!--            End Footer-->


  </div>
</div>

<!--    End Body Content-->


<!--    Foundation Scripts-->
<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>
<script src="js/app.js"></script>

</body>

</html>
