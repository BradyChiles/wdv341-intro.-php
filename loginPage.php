<?php
  session_start();
?>

<!DOCTYPE html>

<html>

  <head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Login Page</title>

    <!-- Bootstrap 4 -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <style>

      #loginHeader{
        text-align: center;
        width: 30%;
        margin: 150px auto auto auto;
      }

      form{
        width: 30%;
        margin: 30px auto;
      }

      .loginField{
        display: block;
        margin: auto;
        margin-bottom: 15px;
      }

      #submitButton{
        display: block;
        margin: auto;
      }

      #loginErrorMessage{
        text-align: center;
        color: red;
        font-size: 20px;
      }

    </style>

  </head>

  <body>

    <?php
      $_SESSION['validUser'] = false;
      $loginErrorMessage = "";
      $username = "";
      $password = "";

      if(isset($_POST["submit"])){
        if($_POST['username'] != "" AND $_POST['password'] != ""){

          include 'connectPDO.php';

          if($connectionSuccess == "Connected successfully"){

            $username = $_POST['username'];
            $password = $_POST['password'];

            // prepare sql and bind parameters
            $stmt = $conn->prepare("SELECT * FROM event_user WHERE event_user_name = :username AND event_user_password = :password");
            $stmt->bindValue(':username', $username, PDO::PARAM_STR);
            $stmt->bindValue(':password', $password, PDO::PARAM_STR);

            if($stmt->execute()){
              $result = $stmt->fetch();
              if($result){
                $_SESSION['validUser'] = true;
              }
            }
          }
        }
        if($_SESSION['validUser'] == false){
          $loginErrorMessage = "Incorrect username or password";
        }
      }


      if($_SESSION['validUser'] != true){
    ?>

        <div id="loginContent">
          <h1 id="loginHeader">Login</h1>
          <form id="loginForm" method="post" action="loginPage.php">
            <p id="loginErrorMessage"><?php echo $loginErrorMessage; ?></p>
            <input type="text" id="username" name="username" class="loginField" value="<?php echo $username; ?>"/>
            <input type="text" id="password" name="password" class="loginField" value="<?php echo $password; ?>"/>
            <input type="submit" id="submitButton" name="submit" value="Submit"/>
          </form>
        </div>

    <?php
      }else{
        header('Location: selectEvents.php');
      }
    ?>

  </body>

</html>