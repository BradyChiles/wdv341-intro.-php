<?php
	session_start();

	if($_SESSION['validUser'] != true){
		header('Location: loginPage.php');
	}

	include 'connectPDO.php';

	$recordID = $_GET['recordID'];

	if($connectionSuccess == "Connected successfully"){

			// prepare sql and bind parameters
		$stmt = $conn->prepare("DELETE FROM wdv341_event WHERE event_id = " . $recordID);
		$_SESSION['deleteSuccess'] = $stmt->execute();
		header('Location: http://renegaderaconteur.net/php/selectEvents.php');
	}

?>