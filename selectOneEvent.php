<!DOCTYPE html>
<html>

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Select Events</title>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

		<?php

			include 'connectPDO.php';

			if($connectionSuccess == "Connected successfully"){

				// prepare sql and bind parameters
			    $stmt = $conn->prepare("SELECT * FROM wdv341_event WHERE event_name = 'Paper: The Truth'");
			    $stmt->execute();
			}

	   ?>

	</head>


	<body>

		<table border='1'>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Description</th>
				<th>Presenter</th>
				<th>Date</th>
				<th>Time</th>
			<tr>

			<?php 

				$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
				foreach($stmt->fetchAll() as $row){
					echo "<tr>";
						echo "<td>" . $row['event_id'] . "</td>";
						echo "<td>" . $row['event_name'] . "</td>";	
						echo "<td>" . $row['event_description'] . "</td>";
						echo "<td>" . $row['event_presenter'] . "</td>";
						echo "<td>" . $row['event_date'] . "</td>";
						echo "<td>" . $row['event_time'] . "</td>";	
					echo "</tr>";
				}

			?>

		</table>

	</body>

</html>