<?php
	include 'Emailer.php';

	$confirmationEmail = new Emailer();
	$contactEmail = new Emailer();

	$confirmationEmail->setSendTo($_POST["email"]);
	$confirmationEmail->setSentFrom("contact@renegaderaconteur.net");
	$confirmationEmail->setEmailSubject("Confirmation from Brady Chiles");
	$confirmationEmail->setEmailMessage("Hello, " . $_POST['name'] . "<br/> I have received your information, and will respond as soon as possible.");
	$confirmationEmail->sendEmail();

	$contactEmail->setSendTo("bmchiles@dmacc.edu");
	$contactEmail->setSentFrom("contact@renegaderaconteur.net");
	$contactEmail->setEmailSubject("Message from Portfolio");
	$contactEmail->setEmailMessage("Name: " . $_POST['name'] . "<br/>" . "Email: " . $_POST['email'] . "<br/>" . "Message: " . $_POST['message']);
	$contactEmail->sendEmail();

	if(isset($_REQUEST["../index.html"])){
      header("Location: {$_REQUEST["../index.html"]}");
  }else if(isset($_SERVER["HTTP_REFERER"])){
      header("Location: {$_SERVER["HTTP_REFERER"]}");
  }else{
       /* some fallback, maybe redirect to index.php */
  }
?>