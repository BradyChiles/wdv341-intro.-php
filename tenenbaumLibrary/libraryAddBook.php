<?php
session_start();

if($_SESSION['validUser'] != true){
  header('Location: libraryLoginPage.php');
}
?>

<!doctype html>
<html class="no-js" lang="en" dir="ltr">

<head>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Tenenbaum Add Book</title>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!--      Foundation Styles-->
  <link rel="stylesheet" href="css/foundation.css">
  <link rel="stylesheet" href="css/app.css">


  <!--      Icon Library-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- JQuery UI -->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <!--      Custom Styles-->
  <link rel="stylesheet" href="navigationBarStyles.css">
  <link rel="stylesheet" href="mainStyles.css">

  <!--      Favicon-->
  <link rel="icon" href="images/book-logo-design-vector-15225895_clipped_rev_1.png">

  <style>

  body, html{
    height: 100%;
    width: 100%;
  }

  #addBookHeader{
    margin: 70px auto 30px auto;
    text-align: center;
  }

  form{
    margin: auto auto 100px auto;
    width: 40%;
  }

  table thead, table tbody, table tfoot{
    background-color: rgba(0,0,0,0);
  }

  table tbody tr:nth-child(even){
    background-color: rgba(0,0,0,0);
  }

  table tbody tr:nth-child(odd){
    background-color: rgba(0,0,0,0);
  }

  table tbody th, table tbody td{
    padding: 0 25px;
  }

  .formButton{
    border-radius: 7px;
    display: block;
    font-size: 18px;
    height: 40px;
    margin: 15px auto;
    -webkit-appearance: button;
    width: 110px;
  }

  [type=text]{
    margin: 0;
  }

  .formLabel{
    display: block;
    margin-top: 20px;
  }

  .error{
    color: red;
    display: block;
    margin: 0;
  }

  #addMessage{
    color: green;
    text-align: center;
  }

</style>

<?php

include 'connectPDO.php';

if($connectionSuccess == true){

        // prepare sql and bind parameters
  $stmt = $conn->prepare("INSERT INTO library_books (book_title, book_author, book_isbn, book_publish_date, book_add_date) 
    VALUES (:bookTitle, :bookAuthor, :bookISBN, :bookPublish, :bookAdd)");
  $stmt->bindParam(':bookTitle', $bookTitle);
  $stmt->bindParam(':bookAuthor', $bookAuthor);
  $stmt->bindParam(':bookISBN', $bookISBN);
  $stmt->bindParam(':bookPublish', $bookPublish);
  $stmt->bindParam(':bookAdd', $bookAdd);

        //Variables
  $bookTitle = "";
  $bookAuthor = "";
  $bookISBN = "";
  $bookPublish = "";
  $bookAdd = date("Y-m-d");

        //error variables
  $bookTitleError = "";
  $bookAuthorError = "";
  $bookISBNError = "";
  $bookPublishError = "";
  $captchaError = "";

  $validForm = true;
  $message = "";


  function validateBookTitle($bookTitle){
    global $validForm, $bookTitleError; 
    $bookTitleError = "";

    if($bookTitle == ""){
      $validForm = false;
      $bookTitleError = "Book Title is required";
    }
  }

  function validateBookAuthor($bookAuthor){
    global $validForm, $bookAuthorError;  
    $bookAuthorError = "";

    if($bookAuthor == ""){
      $validForm = false;
      $bookAuthorError = "Author is required";
    }else if(!preg_match('/^[A-Za-z0-9 ,.-]*[A-Za-z0-9][A-Za-z0-9 ,.-]*$/', $bookAuthor)){
      $validForm = false;
      $bookAuthorError = "Author cannot contain special characters";
    }
  }

  function validateBookISBN($bookISBN){
    global $validForm, $bookISBNError;  
    $bookISBNError = "";

    if($bookISBN == ""){
      $validForm = false;
      $bookISBNError = "ISBN is required";
    }else if(!preg_match('/\b(?:ISBN(?:: ?| ))?((?:97[89])?\d{9}[\dx])\b/i', $bookISBN)){
      $validForm = false;
      $bookISBNError = "ISBN did not match necessary format ()";
    }
  }

  function validatePublishDate($publishDate){
    global $validForm, $bookPublishError; 
    $bookPublishError = "";

    if($publishDate == ""){
      $validForm= false;
      $bookPublishError = "Publish Date is required";
    }
  }

        // // reCaptcha
        // function validateRecaptcha($captcha){
        //  global $validForm, $captchaError;
        //  $captchaError = "";

        //  $secretKey = "6Le3REoUAAAAAIVoaYSGnA23Jcft4Ey5IzvojUIo";
        //  $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha);
        //  $responseKeys = json_decode($response,true);

        //  if($responseKeys["success"] != true) {
        //    $validForm = false;
        //    $captchaError = "Please complete the reCaptcha";
        //  }

        //  grecaptcha.reset();
        // }

  if(isset($_POST["submit"]))
  {

    $bookTitle = $_POST['book_title'];
    $bookAuthor = $_POST['book_author'];
    $bookISBN = $_POST['book_isbn'];
    //Correct format for database
    if($_POST['book_publish_date'] != ""){
      $timestamp = strtotime($_POST['book_publish_date']);
      $bookPublish = date("Y-m-d", $timestamp);
    }

          // if(isset($_POST['g-recaptcha-response'])){
          //  $captcha = $_POST['g-recaptcha-response'];
          // }

    $validForm = true;

    validateBookTitle($bookTitle);
    validateBookAuthor($bookAuthor);
    validateBookISBN($bookISBN);
    validatePublishDate($bookPublish);
          // validateRecaptcha($captcha);


    if($validForm == false){
      $message = "The form is incomplete";
    }else{
      $stmt->execute();

      $message = "Book Added!";
      $eventName = "";
      $eventDescription = "";
      $eventPresenter = "";
      $eventDate = "";
      $eventTime = "";

      $conn = null;
    }

  }
}

?>

<script>
  $(document).ready( function(){

    $('#resetForm').click(function(){
      $("#book_title").val("");
      $("#book_author").val("");
      $("#book_isbn").val("");
      $("#book_publish_date").val("");
      $('.error').html("");
    });

    $( function() {
      $( "#book_publish_date" ).datepicker();
    });

  });


</script>

</head>

<body>
  <div class="off-canvas-wrapper">

    <!--          Begin Navigation-->

    <div class="off-canvas position-right" id="offCanvas" data-off-canvas>

      <!-- Menu -->
      <ul class="vertical menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="#">Our Collection</a></li>
        <li><a href="#">Events &amp; News</a></li>
        <li><a href="#">Locations &amp; Hours</a></li>
        <li><a href="#">Donate</a></li>
        <li><a href="libraryContactPage.php">Contact</a></li>
        <?php
          if(isset($_SESSION['validUser'])){
            if($_SESSION['validUser']){
              echo "<li><a href='libraryBookDisplay.php'>Book Inventory</a></li>";
              echo "<li><a href='libraryAddBook.php'>Add Book</a></li>";
              echo "<li><a href='libraryLogoutPage.php'>Logout</a></li>";
            }else{
              echo "<li><a href='libraryLoginPage.php'>Admin Login</a></li>";
            }
          }
        ?>
      </ul>

    </div>

    <!--          End Navigation-->


    <!--          Begin Body Content-->
    <div class="off-canvas-content" data-off-canvas-content>  
      <div id="menuToggle" data-toggle="offCanvas">
       <button type="button" class="menu-icon dark" ></button>
     </div>


     <!--            Begin Header-->

     <div id="header">
      <h1 id="mainHeaderText">Tenenbaum Royal Library</h1>
    </div>

    <!--            End Header-->

    <h2 id="addBookHeader">Add Book</h2>
    
    
    <form id="bookForm" method="post" action="libraryAddBook.php">

      <p id="addMessage"><?php echo $message; ?></p>

      <table>
        <tr>
          <td><span class="formLabel">Book Title:</span><input type="text" id="book_title" name="book_title" value="<?php echo $bookTitle; ?>"/><span class="error" id="bookTitleError"><?php echo($bookTitleError);?></span></td>
        </tr>
        <tr>
          <td><span class="formLabel">Author:</span><input type="text" id="book_author" name="book_author" value="<?php echo $bookAuthor; ?>"/><span class="error" id="bookAuthorError"><?php echo($bookAuthorError);?></span></td>
        </tr>
        <tr>
          <td><span class="formLabel">ISBN:</span><input type="text" id="book_isbn" name="book_isbn" value="<?php echo $bookISBN; ?>" /><span class="error" id="bookISBNError"><?php echo($bookISBNError);?></span></td>
        </tr>
        <tr>
          <td><span class="formLabel">Publish Date:</span><input type="text" id="book_publish_date" name="book_publish_date" value="<?php echo $bookPublish ?>" /><span class="error" id="bookPublishError"><?php echo($bookPublishError); ?></span></td>
        </tr>
          
        <!-- <tr>
          <td><div class="g-recaptcha" data-sitekey="6Le3REoUAAAAAEJ8etrsYzKL7ZM9ZwMRldJCQ8tz"></div>
          <span class="error"><?php echo $captchaError; ?></span></td>

          <input type="hidden" id="submitConfirm" name="submitConfirm" value="submitConfirm"/>
        </tr> -->
        <tr>
          <td>
            <input type="submit" id="addBook" name="submit" class="formButton" value="Add Book" />
            <input type="button" id="resetForm" name="reset" class="formButton" value="Reset Form"/>
          </td>
        </tr>
      </table>
    </form>

    <!--            Begin Footer-->

    <div id="footer">
      <div id="libraryHours" class="grid-x grid-padding-x">

        <div class="large-offset-2 large-2 medium-6 cell">
          <a href="#">Central</a> <br>
          1000 Grand Avenue, 50309 <br>
          <span class="boldText">Mon – Wed:</span> 9 AM – 8 PM <br> 
          <span class="boldText">Thu – Fri:</span> 9 AM – 6 PM <br>
          <span class="boldText">Sat:</span> Closed <br>
          <span class="boldText">Sun:</span> 1 PM - 5 PM [Sep - May] <br>
          <span class="boldText">Sun:</span> Closed [Jun - Aug]
        </div>

        <div class="large-2 medium-6 cell">
          <a href="#">Forest</a> <br>
          1326 Forest Avenue, 50314 <br>
          <span class="boldText">Mon:</span> 10 AM – 8 PM <br>
          <span class="boldText">Tue – Wed:</span> 10 AM – 6 PM <br>
          <span class="boldText">Thu:</span> 10 AM – 8 PM <br>
          <span class="boldText">Fri:</span> Closed <br>
          <span class="boldText">Sat:</span> 10 AM – 5 PM <br>
          <span class="boldText">Sun:</span> Closed
        </div>

        <div class="large-2 medium-6 cell">
          <a href="#">South Side</a> <br>
          1111 Porter Avenue, 50315 <br>
          <span class="boldText">Mon:</span> 10 AM – 8 PM <br>
          <span class="boldText">Tue:</span> Closed <br>
          <span class="boldText">Wed:</span> 10 AM – 8 PM <br>
          <span class="boldText">Thu – Fri:</span> 10 AM – 6 PM <br>
          <span class="boldText">Sat:</span> 10 AM – 5 PM <br>
          <span class="boldText">Sun:</span> Closed
        </div>

        <div class="large-2 medium-6 cell">
          <a href="#">East Side</a> <br>
          2559 Hubbell Avenue, 50317 <br>
          <span class="boldText">Mon – Tue:</span> 10 AM – 8 PM <br>
          <span class="boldText">Wed:</span> Closed <br>
          <span class="boldText">Thu – Fri:</span> 10 AM – 6 PM <br>
          <span class="boldText">Sat:</span> 10 AM – 5 PM <br>
          <span class="boldText">Sun:</span> Closed
        </div>
      </div>

      <div id="socialMediaIcons">
        <a href="#" class="fa fa-facebook"></a>
        <a href="#" class="fa fa-twitter"></a>
        <a href="#" class="fa fa-instagram"></a>
        <a href="#" class="fa fa-snapchat-ghost"></a>
        <a href="#" class="fa fa-youtube"></a>
      </div>

    </div>

    <!--            End Footer-->


  </div>
</div>

<!--    End Body Content-->

<!--    Foundation Scripts-->
  <!-- <script src="js/vendor/jquery.js"></script> -->
  <script src="js/vendor/what-input.js"></script>
  <script src="js/vendor/foundation.js"></script>
  <script src="js/app.js"></script>


</body>

</html>