<?php
  session_start();
?>

<!doctype html>
<html class="no-js" lang="en" dir="ltr">

<head>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Tenenbaum Login</title>

  <!--      Foundation Styles-->
  <link rel="stylesheet" href="css/foundation.css">
  <link rel="stylesheet" href="css/app.css">

  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <!--      Icon Library-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!--      Custom Styles-->
  <link rel="stylesheet" href="navigationBarStyles.css">
  <link rel="stylesheet" href="mainStyles.css">

  <!--      Favicon-->
  <link rel="icon" href="images/book-logo-design-vector-15225895_clipped_rev_1.png">

  <style>

  body, html{
    height: 100%;
    width: 100%;
  }

  #loginContent{
    margin-bottom: 100px;
  }

  #loginHeader{
    text-align: center;
    width: 30%;
    margin: 75px auto auto auto;
  }

  form{
    width: 30%;
    margin: 30px auto;
  }

  .loginField{
    display: block;
    margin: auto;
    margin-bottom: 15px;
  }

  #submitButton{
    display: block;
    font-size: 20px;
    height: 45px;
    margin: auto;
    width: 85px;
  }

  #loginErrorMessage{
    text-align: center;
    color: red;
    font-size: 20px;
  }

</style>

</head>

<body>
  <div class="off-canvas-wrapper">

    <!--          Begin Navigation-->

    <div class="off-canvas position-right" id="offCanvas" data-off-canvas>

      <!-- Menu -->
      <ul class="vertical menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="#">Our Collection</a></li>
        <li><a href="#">Events &amp; News</a></li>
        <li><a href="#">Locations &amp; Hours</a></li>
        <li><a href="#">Donate</a></li>
        <li><a href="libraryContactPage.php">Contact</a></li>
        <?php
          if(isset($_SESSION['validUser'])){
            if($_SESSION['validUser']){
              echo "<li><a href='libraryBookDisplay.php'>Book Inventory</a></li>";
              echo "<li><a href='libraryAddBook.php'>Add Book</a></li>";
              echo "<li><a href='libraryLogoutPage.php'>Logout</a></li>";
            }else{
              echo "<li><a href='libraryLoginPage.php'>Admin Login</a></li>";
            }
          }
        ?>
      </ul>

    </div>

    <!--          End Navigation-->


    <!--          Begin Body Content-->
    <div class="off-canvas-content" data-off-canvas-content>  
      <div id="menuToggle" data-toggle="offCanvas">
       <button type="button" class="menu-icon dark" ></button>
     </div>


     <!--            Begin Header-->

     <div id="header">
      <h1 id="mainHeaderText">Tenenbaum Royal Library</h1>
    </div>

    <!--            End Header-->


    <?php
    $_SESSION['validUser'] = false;
    $loginErrorMessage = "";
    $username = "";
    $password = "";

    if(isset($_POST["submit"])){
      $username = $_POST['username'];
      $password = $_POST['password'];

      if($_POST['username'] != "" AND $_POST['password'] != ""){

        include 'connectPDO.php';

        $username = $_POST['username'];
        $password = $_POST['password'];

        if($connectionSuccess == true){

            // prepare sql and bind parameters
          $stmt = $conn->prepare("SELECT * FROM library_admins WHERE admin_username = :username AND admin_password = :password");
          $stmt->bindValue(':username', $username, PDO::PARAM_STR);
          $stmt->bindValue(':password', $password, PDO::PARAM_STR);

          if($stmt->execute()){
            $result = $stmt->fetch();
            if($result){
              $_SESSION['validUser'] = true;
            }else{
              $loginErrorMessage = "Incorrect username or password";
            }
          }
        }
      }else{
        $loginErrorMessage = "Please enter a username and password";
      }
    }


    if($_SESSION['validUser'] != true){
      ?>

      <div id="loginContent">
        <h1 id="loginHeader">Login</h1>
        <form id="loginForm" method="post" action="libraryLoginPage.php">
          <p id="loginErrorMessage"><?php echo $loginErrorMessage; ?></p>
          <input type="text" id="username" name="username" class="loginField" value="<?php echo $username; ?>"/>
          <input type="text" id="password" name="password" class="loginField" value="<?php echo $password; ?>"/>
          <input type="submit" id="submitButton" name="submit" value="Submit"/>
        </form>
      </div>

      <?php
    }else{
      header('Location: libraryBookDisplay.php');
    }
    ?>


    <!--            Begin Footer-->

    <div id="footer">
      <div id="libraryHours" class="grid-x grid-padding-x">

        <div class="large-offset-2 large-2 medium-6 cell">
          <a href="#">Central</a> <br>
          1000 Grand Avenue, 50309 <br>
          <span class="boldText">Mon – Wed:</span> 9 AM – 8 PM <br> 
          <span class="boldText">Thu – Fri:</span> 9 AM – 6 PM <br>
          <span class="boldText">Sat:</span> Closed <br>
          <span class="boldText">Sun:</span> 1 PM - 5 PM [Sep - May] <br>
          <span class="boldText">Sun:</span> Closed [Jun - Aug]
        </div>

        <div class="large-2 medium-6 cell">
          <a href="#">Forest</a> <br>
          1326 Forest Avenue, 50314 <br>
          <span class="boldText">Mon:</span> 10 AM – 8 PM <br>
          <span class="boldText">Tue – Wed:</span> 10 AM – 6 PM <br>
          <span class="boldText">Thu:</span> 10 AM – 8 PM <br>
          <span class="boldText">Fri:</span> Closed <br>
          <span class="boldText">Sat:</span> 10 AM – 5 PM <br>
          <span class="boldText">Sun:</span> Closed
        </div>

        <div class="large-2 medium-6 cell">
          <a href="#">South Side</a> <br>
          1111 Porter Avenue, 50315 <br>
          <span class="boldText">Mon:</span> 10 AM – 8 PM <br>
          <span class="boldText">Tue:</span> Closed <br>
          <span class="boldText">Wed:</span> 10 AM – 8 PM <br>
          <span class="boldText">Thu – Fri:</span> 10 AM – 6 PM <br>
          <span class="boldText">Sat:</span> 10 AM – 5 PM <br>
          <span class="boldText">Sun:</span> Closed
        </div>

        <div class="large-2 medium-6 cell">
          <a href="#">East Side</a> <br>
          2559 Hubbell Avenue, 50317 <br>
          <span class="boldText">Mon – Tue:</span> 10 AM – 8 PM <br>
          <span class="boldText">Wed:</span> Closed <br>
          <span class="boldText">Thu – Fri:</span> 10 AM – 6 PM <br>
          <span class="boldText">Sat:</span> 10 AM – 5 PM <br>
          <span class="boldText">Sun:</span> Closed
        </div>
      </div>

      <div id="socialMediaIcons">
        <a href="#" class="fa fa-facebook"></a>
        <a href="#" class="fa fa-twitter"></a>
        <a href="#" class="fa fa-instagram"></a>
        <a href="#" class="fa fa-snapchat-ghost"></a>
        <a href="#" class="fa fa-youtube"></a>
      </div>

    </div>

    <!--            End Footer-->


  </div>
</div>

<!--    End Body Content-->


<!--    Foundation Scripts-->
<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>
<script src="js/app.js"></script>

</body>

</html>
